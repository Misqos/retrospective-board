package pl.edu.agh.jtp2.retrospective.domain.board;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class BoardTest {

    @Test
    public void testSetGetId() throws Exception {
        //given
        Board board = new Board();
        int random = RandomUtils.nextInt();

        //when
        board.setId(random);

        //then
        assertThat(board.getId()).isEqualTo(random);
    }

    @Test
    public void testSetGetTitle() throws Exception {
        //given
        Board board = new Board();
        String title = RandomStringUtils.random(10);

        //when
        board.setTitle(title);

        //then
        assertThat(board.getTitle()).isEqualTo(title);
    }

    @Test
    public void testSetGetStatus() throws Exception {
        //given
        Board board = new Board();
        Status status = Status.ADDING;

        //when
        board.setStatus(status);

        //then
        assertThat(board.getStatus()).isEqualTo(status);
    }

    @Test
    public void testSetGetUsers() throws Exception {
        //given
        Board board = new Board();
        int random = RandomUtils.nextInt();

        //when
        board.setUsers(random);

        //then
        assertThat(board.getUsers()).isEqualTo(random);
    }
}