package pl.edu.agh.jtp2.retrospective.service.note;

import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;

import java.util.List;

/**
 * Created by misqos on 10.08.14.
 */
public interface NoteGroupService {
    public int nextId();

    public void insert(NoteGroup group);

    public List<NoteGroup> get(int boardId);

    public NoteGroup getSingle(int groupId);

    public void update(NoteGroup group);

    public List<NoteGroup> get(NoteType type, int boardId);

    public NoteGroup merge(NoteGroup group1, NoteGroup group2);

    public void remove(NoteGroup group);
}
