package pl.edu.agh.jtp2.retrospective.service.token;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

/**
 * Created by misqos on 09.08.14.
 */
public class SimpleTokenMachine implements TokenMachine {
    @Autowired
    UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private String generateUserToken() {
        return "U" + RandomStringUtils.randomAlphanumeric(7);
    }

    private String generateAdminToken() {
        return "A" + RandomStringUtils.randomAlphanumeric(7);
    }

    private boolean tokenIsDuplicated(String token) {
        return userService.get(token) != null;
    }

    @Override
    public String getAdminToken() {
        String token = generateAdminToken();
        while (tokenIsDuplicated(token)) {
            token = generateAdminToken();
        }
        return token;
    }

    @Override
    public String getUserToken() {
        String token = generateUserToken();
        while (tokenIsDuplicated(token)) {
            token = generateUserToken();
        }
        return token;
    }
}
