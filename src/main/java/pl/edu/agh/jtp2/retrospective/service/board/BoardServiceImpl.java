package pl.edu.agh.jtp2.retrospective.service.board;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.board.BoardDAO;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public class BoardServiceImpl implements BoardService {
    @Autowired
    BoardDAO boardDAO;

    public void setBoardDAO(BoardDAO boardDAO) {
        this.boardDAO = boardDAO;
    }

    @Override
    public int nextId() {
        if (boardDAO.findAllFromNewest(1).isEmpty()) return 100; //id starting from 1 is boring.
        return boardDAO.findAllFromNewest().get(0).getId() + 1;
    }

    @Override
    public void insert(Board board) {
        boardDAO.insert(board);
    }

    @Override
    public List<Board> get() {
        return boardDAO.findAll();
    }

    @Override
    public Board get(int id) {
        return boardDAO.findById(id);
    }

    @Override
    public void update(Board board) {
        boardDAO.update(board);
    }
}
