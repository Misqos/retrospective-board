package pl.edu.agh.jtp2.retrospective.service.board;

import pl.edu.agh.jtp2.retrospective.domain.board.Board;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public interface BoardService {
    public int nextId();

    public void insert(Board board);

    public List<Board> get();

    public Board get(int id);

    public void update(Board board);
}
