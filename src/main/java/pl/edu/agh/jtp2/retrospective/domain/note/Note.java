package pl.edu.agh.jtp2.retrospective.domain.note;

/**
 * Created by misqos on 05.08.14.
 */
public class Note {
    private int id;
    private int groupId;
    private String text;
    private int userId;
    private NoteType noteType;

    public Note(int id, int groupId, int userId, NoteType type, String text) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.noteType = type;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public NoteType getNoteType() {
        return noteType;
    }

    public void setNoteType(NoteType noteType) {
        this.noteType = noteType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;

        Note note = (Note) o;

        if (groupId != note.groupId) return false;
        if (id != note.id) return false;
        if (userId != note.userId) return false;
        if (noteType != note.noteType) return false;
        if (text != null ? !text.equals(note.text) : note.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + groupId;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + userId;
        result = 31 * result + (noteType != null ? noteType.hashCode() : 0);
        return result;
    }
}
