package pl.edu.agh.jtp2.retrospective.dao.board;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;
import pl.edu.agh.jtp2.retrospective.domain.board.Status;
import pl.edu.agh.jtp2.retrospective.domain.voting.VotingStrategies;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by misqos on 06.08.14.
 */
public class BoardDAOImpl implements BoardDAO {
    @Autowired
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(Board board) {
        String sql = "INSERT INTO Boards " +
                "(id, users, title, status, voting) VALUES (?, ?, ?, ?, ?)";

        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, board.getId());
            ps.setInt(2, board.getUsers());
            ps.setString(3, board.getTitle());
            ps.setString(4, String.valueOf(board.getStatus()));
            ps.setString(5, String.valueOf(board.getVoting()));
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Board findById(int id) {
        String sql = "SELECT * FROM Boards WHERE id = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            Board board = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                board = new Board(
                        rs.getInt("id"),
                        rs.getInt("users"),
                        rs.getString("title"),
                        Enum.valueOf(Status.class, rs.getString("status")),
                        Enum.valueOf(VotingStrategies.class, rs.getString("voting"))
                );
            }
            rs.close();
            ps.close();
            return board;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Board> findAll() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT * FROM Boards";

        List<Board> boards = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            Board board = new Board(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("users"))),
                    String.valueOf(row.get("title")),
                    Enum.valueOf(Status.class, String.valueOf(row.get("status"))),
                    Enum.valueOf(VotingStrategies.class, String.valueOf(row.get("voting")))
            );
            boards.add(board);
        }
        return boards;
    }

    @Override
    public List<Board> findAllFromNewest() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT * FROM Boards " +
                "ORDER BY id DESC";

        List<Board> boards = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            Object voting = String.valueOf(row.get("voting"));
            if (voting == null) {
                voting = VotingStrategies.LIKEDISLIKE;
            } else {
                voting = Enum.valueOf(VotingStrategies.class, (String) voting);
            }
            Board board = new Board(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("users"))),
                    String.valueOf(row.get("title")),
                    Enum.valueOf(Status.class, String.valueOf(row.get("status"))),
                    (VotingStrategies) voting
            );
            boards.add(board);
        }
        return boards;
    }

    @Override
    public void update(Board board) {
        String sql = "UPDATE Boards SET users = ?, title = ?, status = ?, voting = ? WHERE id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, board.getUsers(), board.getTitle(), String.valueOf(board.getStatus()), String.valueOf(board.getVoting()), board.getId());
    }

    @Override
    public List<Board> findAllFromNewest(int limit) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT * FROM Boards " +
                "ORDER BY id DESC";

        List<Board> boards = new ArrayList<>();
        jdbcTemplate.setFetchSize(limit);
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            Object voting = String.valueOf(row.get("voting"));
            if (voting == null) {
                voting = VotingStrategies.LIKEDISLIKE;
            } else {
                voting = Enum.valueOf(VotingStrategies.class, (String) voting);
            }
            Board board = new Board(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("users"))),
                    String.valueOf(row.get("title")),
                    Enum.valueOf(Status.class, String.valueOf(row.get("status"))),
                    (VotingStrategies) voting
            );
            boards.add(board);
        }
        return boards;
    }
}
