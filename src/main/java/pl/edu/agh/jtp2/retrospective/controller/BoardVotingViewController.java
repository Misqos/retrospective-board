package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNote;
import pl.edu.agh.jtp2.retrospective.helpers.ViewNoteGroup;
import pl.edu.agh.jtp2.retrospective.service.board.BoardService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteService;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;
import pl.edu.agh.jtp2.retrospective.service.voting.InHandVotingService;
import pl.edu.agh.jtp2.retrospective.service.voting.LikeDislikeVotingService;
import pl.edu.agh.jtp2.retrospective.service.voting.VotingService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by misqos on 13.08.14.
 */
@Controller
public class BoardVotingViewController {
    @Autowired
    UserService userService;

    @Autowired
    BoardService boardService;

    @Autowired
    NoteGroupService noteGroupService;

    @Autowired
    NoteService noteService;

    @Autowired
    LikeDislikeVotingService likeDislikeVotingService;

    @Autowired
    InHandVotingService inHandVotingService;

    VotingService votingService;

    @RequestMapping(value = "/voting/{token}")
    public String voting(@PathVariable String token, ModelMap model) {
        User user = userService.get(token);
        Board board = boardService.get(user.getBoardId());

        boolean displayVotesLeft = false;

        switch (board.getVoting()) {
            case INHAND:
                votingService = inHandVotingService;
                displayVotesLeft = true;
                break;
            case LIKEDISLIKE:
                votingService = likeDislikeVotingService;
                break;
        }

        List<NoteGroup> bgroups = noteGroupService.get(board.getId());
        List<ViewNoteGroup> groups = new ArrayList<>();
        for (NoteGroup g : bgroups) {
            ViewNoteGroup group = new ViewNoteGroup(g, votingService, user);
            List<FrontNote> nots = noteService.get(g.getId()).stream().map(note ->
                    new FrontNote(note, userService)).collect(Collectors.toList());
            group.setNotes(nots);
            groups.add(group);
        }
        groups.sort((o1, o2) -> o2.getPriority() - o1.getPriority());

        int votesLeft = 0;
        if (displayVotesLeft) votesLeft = 20 - votingService.getUserVotes(user);

        model.addAttribute("displayVotesLeft", displayVotesLeft);
        model.addAttribute("votesLeft", votesLeft);
        model.addAttribute("noNotes", groups.isEmpty());
        model.addAttribute("groups", groups);


        model.addAttribute("token", token);
        model.addAttribute("title", board.getTitle());
        model.addAttribute("username", user.getName());
        model.addAttribute("admin", token.startsWith("A"));
        model.addAttribute("pagetitle", "Retrospective Board - " + user.getName() + " - voting stage");
        return "voting";
    }

    @RequestMapping(value = "/voting/{token}/up/{groupId}")
    public String voteUp(@PathVariable String token, @PathVariable int groupId) {
        User user = userService.get(token);
        NoteGroup group = noteGroupService.getSingle(groupId);

        votingService.upVote(user, group);
        return "redirect:/voting/" + token;
    }

    @RequestMapping(value = "/voting/{token}/down/{groupId}")
    public String voteDown(@PathVariable String token, @PathVariable int groupId) {
        User user = userService.get(token);
        NoteGroup group = noteGroupService.getSingle(groupId);

        votingService.downVote(user, group);
        return "redirect:/voting/" + token;
    }

    @RequestMapping(value = "/voting/{token}/clear/{groupId}")
    public String clear(@PathVariable String token, @PathVariable int groupId) {
        User user = userService.get(token);
        NoteGroup group = noteGroupService.getSingle(groupId);

        votingService.reset(user, group);
        return "redirect:/voting/" + token;
    }
}
