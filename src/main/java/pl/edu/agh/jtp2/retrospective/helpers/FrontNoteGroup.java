package pl.edu.agh.jtp2.retrospective.helpers;

import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;

import java.util.List;

/**
 * Created by misqos on 13.08.14.
 */
public class FrontNoteGroup extends NoteGroup implements Comparable<FrontNoteGroup> {
    private List<FrontNote> notes;

    public FrontNoteGroup(NoteGroup group) {
        super(group.getId(), group.getBoardId(), group.getType(), group.getPriority());
    }

    public List<FrontNote> getNotes() {
        return notes;
    }

    public void setNotes(List<FrontNote> notes) {
        this.notes = notes;
    }

    @Override
    public int compareTo(FrontNoteGroup o) {
        return Integer.compare(o.getPriority(), getPriority());
    }
}
