package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;
import pl.edu.agh.jtp2.retrospective.domain.board.Status;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNote;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNoteGroup;
import pl.edu.agh.jtp2.retrospective.service.board.BoardService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteService;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by misqos on 09.08.14.
 */
@Controller
public class BoardAddingViewController {
    @Autowired
    UserService userService;

    @Autowired
    BoardService boardService;

    @Autowired
    NoteGroupService noteGroupService;

    @Autowired
    NoteService noteService;

    @RequestMapping(value = "/board/{token}")
    public String displayBoard(@PathVariable String token, ModelMap model) {
        User user = userService.get(token);
        final Board board1 = boardService.get(user.getBoardId());
        if (board1.getStatus().equals(Status.CLOSED)) {
            if (!isPermitted(token)) {
                return "redirect:/";
            } else {
                return "redirect:/board/" + token + "/report";
            }
        }
        if (board1.getStatus().equals(Status.VOTING)) {
            return "redirect:/voting/" + token;
        }
        if (user.getName().trim().equals("")) {
            return "redirect:/user/" + token + "/name";
        }
        Board board = boardService.get(user.getBoardId());
        model.addAttribute("title", board.getTitle());
        model.addAttribute("token", token);
        List<NoteGroup> bgroups = noteGroupService.get(board.getId());
        List<FrontNoteGroup> groups = new ArrayList<>();
        for (NoteGroup g : bgroups) {
            FrontNoteGroup group = new FrontNoteGroup(g);
            List<FrontNote> nots = noteService.get(g.getId()).stream().map(note ->
                    new FrontNote(note, userService)).collect(Collectors.toList());
            group.setNotes(nots);
            groups.add(group);
        }
        model.addAttribute("noNotes", groups.isEmpty());
        model.addAttribute("groups", groups);
        model.addAttribute("username", user.getName());
        model.addAttribute("admin", token.startsWith("A"));
        model.addAttribute("pagetitle", "Retrospective Board - " + user.getName());
        return "board";
    }

    private boolean isPermitted(String token) {
        return token.startsWith("A");
    }
}
