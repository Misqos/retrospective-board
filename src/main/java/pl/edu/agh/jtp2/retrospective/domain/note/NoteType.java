package pl.edu.agh.jtp2.retrospective.domain.note;

/**
 * Created by misqos on 05.08.14.
 */
public enum NoteType {
    GLAD("Glad", "green"),
    SAD("Sad", "red"),
    NEW_IDEA("New idea", "blue");

    private String description;

    private String color;

    private NoteType(String description, String color) {
        this.description = description;
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public String getColor() {
        return color;
    }
}
