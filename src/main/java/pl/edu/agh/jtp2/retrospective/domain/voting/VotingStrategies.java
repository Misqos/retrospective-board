package pl.edu.agh.jtp2.retrospective.domain.voting;

/**
 * Created by misqos on 09.08.14.
 */
public enum VotingStrategies {
    LIKEDISLIKE("Like / Dislike"),
    INHAND("20 votes in hand");
    private String description;

    private VotingStrategies(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
