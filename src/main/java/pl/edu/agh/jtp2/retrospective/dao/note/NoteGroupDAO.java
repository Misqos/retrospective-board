package pl.edu.agh.jtp2.retrospective.dao.note;

import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public interface NoteGroupDAO {
    public void insert(NoteGroup group);

    public NoteGroup findById(int id);

    public List<NoteGroup> findByBoardId(int boardId);

    public List<NoteGroup> findAllFromNewest();

    public void update(NoteGroup group);

    public List<NoteGroup> findByTypeAndBoardId(NoteType type, int boardId);

    public void remove(NoteGroup group);
}
