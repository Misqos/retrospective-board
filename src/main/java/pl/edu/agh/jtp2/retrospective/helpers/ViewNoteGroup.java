package pl.edu.agh.jtp2.retrospective.helpers;

import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.service.voting.VotingService;

/**
 * Created by misqos on 13.08.14.
 */
public class ViewNoteGroup extends FrontNoteGroup {
    private final VotingService votingService;
    private final User user;

    public ViewNoteGroup(NoteGroup group, VotingService votingService, User user) {
        super(group);
        this.votingService = votingService;
        this.user = user;
    }

    public boolean getCanUpVote() {
        return votingService.canUpVote(user, this);
    }

    public boolean getCanDownVote() {
        return votingService.canDownVote(user, this);
    }

    public boolean getPossibleDownVote() {
        return votingService.possibleDownVote();
    }

    public boolean getUpVoted() {
        return votingService.upVoted(user, this);
    }

    public boolean getDownVoted() {
        return votingService.downVoted(user, this);
    }

    public boolean getTouched() {
        return votingService.isTouched(user, this);
    }
}
