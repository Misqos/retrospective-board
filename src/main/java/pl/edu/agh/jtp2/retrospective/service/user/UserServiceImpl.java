package pl.edu.agh.jtp2.retrospective.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.user.UserDAO;
import pl.edu.agh.jtp2.retrospective.domain.User;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public class UserServiceImpl implements UserService {
    @Autowired
    UserDAO userDAO;

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public int nextId() {
        if (userDAO.findAllFromNewest().isEmpty()) return 100; //id starting from 1 is boring.
        return userDAO.findAllFromNewest().get(0).getId() + 1;
    }

    @Override
    public void insert(User user) {
        userDAO.insert(user);
    }

    @Override
    public List<User> get() {
        return userDAO.findAll();
    }

    @Override
    public User get(int id) {
        return userDAO.findById(id);
    }

    @Override
    public int getBoardId(String userToken) {
        return userDAO.findByToken(userToken).getBoardId();
    }

    @Override
    public int getBoardId(int userId) {
        return userDAO.findById(userId).getBoardId();
    }

    @Override
    public User get(String userToken) {
        return userDAO.findByToken(userToken);
    }

    @Override
    public void update(User user) {
        userDAO.update(user);
    }

    @Override
    public List<User> getByBoardId(int id) {
        return userDAO.findByBoardId(id);
    }
}
