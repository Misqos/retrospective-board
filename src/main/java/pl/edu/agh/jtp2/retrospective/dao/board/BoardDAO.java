package pl.edu.agh.jtp2.retrospective.dao.board;

import pl.edu.agh.jtp2.retrospective.domain.board.Board;

import java.util.List;

/**
 * Created by misqos on 06.08.14.
 */
public interface BoardDAO {
    public void insert(Board board);

    public Board findById(int id);

    public List<Board> findAll();

    public List<Board> findAllFromNewest();

    public List<Board> findAllFromNewest(int limit);

    void update(Board board);
}
