package pl.edu.agh.jtp2.retrospective.domain.note;

/**
 * Created by misqos on 05.08.14.
 */
public class NoteGroup {
    private int id;
    private int boardId;
    private NoteType type;
    private int priority;

    public NoteGroup(int id, int boardId, NoteType type, int priority) {
        this.id = id;
        this.boardId = boardId;
        this.type = type;
        this.priority = priority;
    }

    public NoteGroup(NoteType type, int boardId) {
        this.type = type;
        priority = 0;
        this.boardId = boardId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBoardId() {
        return boardId;
    }

    public NoteType getType() {
        return type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void voteUp() {
        priority++;
    }

    public void voteDown() {
        priority--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NoteGroup)) return false;

        NoteGroup noteGroup = (NoteGroup) o;

        if (boardId != noteGroup.boardId) return false;
        if (id != noteGroup.id) return false;
        if (priority != noteGroup.priority) return false;
        if (type != noteGroup.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + boardId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }
}
