package pl.edu.agh.jtp2.retrospective.service.note;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.note.NoteDAO;
import pl.edu.agh.jtp2.retrospective.domain.note.Note;

import java.util.List;

/**
 * Created by misqos on 10.08.14.
 */
public class NoteServiceImpl implements NoteService {
    @Autowired
    NoteDAO noteDAO;

    public void setNoteDAO(NoteDAO noteDAO) {
        this.noteDAO = noteDAO;
    }

    @Override
    public int nextId() {
        if (noteDAO.findAllFromNewest().isEmpty()) return 100;
        return noteDAO.findAllFromNewest().get(0).getId() + 1;
    }

    @Override
    public void insert(Note note) {
        noteDAO.insert(note);
    }

    @Override
    public List<Note> get(int groupId) {
        return noteDAO.findByGroupId(groupId);
    }

    @Override
    public void update(Note note) {
        noteDAO.update(note);
    }

    @Override
    public String getAuthorName(int id) {
        return null;
    }
}
