package pl.edu.agh.jtp2.retrospective.dao.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.agh.jtp2.retrospective.domain.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by misqos on 09.08.14.
 */
public class UserDAOImpl implements UserDAO {
    @Autowired
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(User user) {
        String sql = "INSERT INTO Users" +
                "(id, token, boardId, nick) VALUES (?, ?, ?, ?)";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, user.getId());
            ps.setString(2, user.getKey());
            ps.setInt(3, user.getBoardId());
            ps.setString(4, user.getName());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> findAll() {
        String sql = "SELECT * FROM Users";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<User> users = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            User user = new User(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    String.valueOf(row.get("token")),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    String.valueOf(row.get("nick"))
            );
            users.add(user);
        }
        return users;
    }

    @Override
    public User findById(int id) {
        String sql = "SELECT * FROM Users WHERE id = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            User user = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getInt("id"),
                        rs.getString("token"),
                        rs.getInt("boardId"),
                        rs.getString("nick")
                );
            }
            rs.close();
            ps.close();
            return user;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User findByToken(String token) {
        String sql = "SELECT * FROM Users WHERE token = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, token);
            User user = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = new User(
                        rs.getInt("id"),
                        rs.getString("token"),
                        rs.getInt("boardId"),
                        rs.getString("nick")
                );
            }
            rs.close();
            ps.close();
            return user;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> findAllFromNewest() {
        String sql = "SELECT * FROM Users " +
                "ORDER BY id DESC";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<User> users = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            User user = new User(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    String.valueOf(row.get("token")),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    String.valueOf(row.get("nick"))
            );
            users.add(user);
        }
        return users;
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE Users SET token = ?, boardId = ?, nick = ? WHERE id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, user.getKey(), user.getBoardId(), user.getName(), user.getId());
    }

    @Override
    public List<User> findByBoardId(int id) {
        String sql = "SELECT * FROM Users WHERE boardId = ? " +
                "ORDER BY id DESC";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<User> users = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, id);
        for (Map row : rows) {
            User user = new User(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    String.valueOf(row.get("token")),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    String.valueOf(row.get("nick"))
            );
            users.add(user);
        }
        return users;
    }
}
