package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

/**
 * Created by misqos on 09.08.14.
 */
@Controller
public class UserController {
    @Autowired
    UserService userService;

    String token;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String user() {
        return "redirect:/";
    }

    @RequestMapping(value = "/user/{token}/name", method = RequestMethod.GET)
    public String name(@PathVariable String token, ModelMap model) {
        this.token = token;
        model.addAttribute("pagetitle", "Retrospective Board - username setting");
        return "setname";
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public String setName(@RequestParam("name") String name, ModelMap model) {
        User user = userService.get(token);
        user.setName(name);
        userService.update(user);
        return "redirect:/board/" + user.getKey();
    }
}
