package pl.edu.agh.jtp2.retrospective.service.note;

import pl.edu.agh.jtp2.retrospective.domain.note.Note;

import java.util.List;

/**
 * Created by misqos on 10.08.14.
 */
public interface NoteService {
    public int nextId();

    public void insert(Note note);

    public List<Note> get(int groupId);

    public String getAuthorName(int id);

    public void update(Note note);
}
