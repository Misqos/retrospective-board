package pl.edu.agh.jtp2.retrospective.service.note;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.note.NoteGroupDAO;
import pl.edu.agh.jtp2.retrospective.domain.note.Note;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;

import java.util.List;

/**
 * Created by misqos on 10.08.14.
 */
public class NoteGroupServiceImpl implements NoteGroupService {
    @Autowired
    NoteGroupDAO noteGroupDAO;

    @Autowired
    NoteService noteService;

    public void setNoteGroupDAO(NoteGroupDAO noteGroupDAO) {
        this.noteGroupDAO = noteGroupDAO;
    }

    @Override
    public int nextId() {
        if (noteGroupDAO.findAllFromNewest().isEmpty()) return 100;
        return noteGroupDAO.findAllFromNewest().get(0).getId() + 1;
    }

    @Override
    public void insert(NoteGroup group) {
        noteGroupDAO.insert(group);
    }

    @Override
    public List<NoteGroup> get(int boardId) {
        return noteGroupDAO.findByBoardId(boardId);
    }

    @Override
    public void update(NoteGroup group) {
        noteGroupDAO.update(group);
    }

    @Override
    public NoteGroup getSingle(int groupId) {
        return noteGroupDAO.findById(groupId);
    }

    @Override
    public List<NoteGroup> get(NoteType type, int boardId) {
        return noteGroupDAO.findByTypeAndBoardId(type, boardId);
    }

    @Override
    public void remove(NoteGroup group) {
        noteGroupDAO.remove(group);
    }

    @Override
    public NoteGroup merge(NoteGroup group1, NoteGroup group2) {
        NoteGroup merged = new NoteGroup(nextId(), group1.getBoardId(), group1.getType(), 0);
        int id = merged.getId();
        List<Note> notes = noteService.get(group1.getId());
        notes.addAll(noteService.get(group2.getId()));
        for (Note note : notes) {
            note.setGroupId(id);
            noteService.update(note);
        }
        remove(group1);
        remove(group2);
        return merged;
    }
}
