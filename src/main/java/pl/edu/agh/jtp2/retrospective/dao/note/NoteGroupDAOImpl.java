package pl.edu.agh.jtp2.retrospective.dao.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by misqos on 09.08.14.
 */
public class NoteGroupDAOImpl implements NoteGroupDAO {
    @Autowired
    DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(NoteGroup group) {
        String sql = "INSERT INTO NoteGroups " +
                "(id, boardId, typo, priority) VALUES (?, ?, ?, ?)";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, group.getId());
            ps.setInt(2, group.getBoardId());
            ps.setString(3, String.valueOf(group.getType()));
            ps.setInt(4, group.getPriority());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public NoteGroup findById(int id) {
        String sql = "SELECT * FROM NoteGroups WHERE id = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            NoteGroup group = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                group = new NoteGroup(
                        rs.getInt("id"),
                        rs.getInt("boardId"),
                        Enum.valueOf(NoteType.class, rs.getString("typo")),
                        rs.getInt("priority")
                );
            }
            rs.close();
            ps.close();
            return group;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<NoteGroup> findByBoardId(int boardId) {
        String sql = "SELECT * FROM NoteGroups WHERE boardId = ? ORDER BY priority DESC";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<NoteGroup> groups = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, boardId);
        for (Map row : rows) {
            NoteGroup group = new NoteGroup(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    Enum.valueOf(NoteType.class, String.valueOf(row.get("typo"))),
                    Integer.parseInt(String.valueOf(row.get("priority")))
            );
            groups.add(group);
        }
        return groups;
    }

    @Override
    public List<NoteGroup> findAllFromNewest() {
        String sql = "SELECT * FROM NoteGroups ORDER BY id DESC";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<NoteGroup> groups = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            NoteGroup group = new NoteGroup(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    Enum.valueOf(NoteType.class, String.valueOf(row.get("typo"))),
                    Integer.parseInt(String.valueOf(row.get("priority")))
            );
            groups.add(group);
        }
        return groups;
    }

    @Override
    public void update(NoteGroup group) {
        String sql = "UPDATE NoteGroups SET boardId = ?, typo = ?, priority = ? WHERE id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getBoardId(), String.valueOf(group.getType()), group.getPriority(), group.getId());
    }

    @Override
    public List<NoteGroup> findByTypeAndBoardId(NoteType type, int boardId) {
        String sql = "SELECT * FROM NoteGroups WHERE boardId = ? AND typo = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<NoteGroup> groups = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, boardId, String.valueOf(type));
        for (Map row : rows) {
            NoteGroup group = new NoteGroup(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("boardId"))),
                    Enum.valueOf(NoteType.class, String.valueOf(row.get("typo"))),
                    Integer.parseInt(String.valueOf(row.get("priority")))
            );
            groups.add(group);
        }
        return groups;
    }

    @Override
    public void remove(NoteGroup group) {
        String sql = "DELETE FROM NoteGroups WHERE id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getId());
    }
}
