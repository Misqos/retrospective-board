package pl.edu.agh.jtp2.retrospective.helpers;

import pl.edu.agh.jtp2.retrospective.domain.note.Note;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

/**
 * Created by misqos on 13.08.14.
 */
public final class FrontNote extends Note {
    private String author;

    public FrontNote(Note note, UserService userService) {
        super(note.getId(), note.getGroupId(), note.getUserId(), note.getNoteType(), note.getText());
        author = userService.get(note.getUserId()).getName();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
