package pl.edu.agh.jtp2.retrospective.domain.board;

import pl.edu.agh.jtp2.retrospective.domain.voting.VotingStrategies;

/**
 * Created by misqos on 05.08.14.
 */
public class Board {
    private int id;
    private String title;
    private int users;
    private Status status;
    private VotingStrategies voting;

    public Board() {
        status = Status.ADDING;
    }

    public Board(int id, int users, String title, Status status, VotingStrategies voting) {
        this.id = id;
        this.users = users;
        this.title = title;
        this.status = status;
        this.voting = voting;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void close() {
        status = Status.CLOSED;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }

    public VotingStrategies getVoting() {
        return voting;
    }

    public void setVoting(VotingStrategies voting) {
        this.voting = voting;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Board)) return false;

        Board board = (Board) o;

        if (id != board.id) return false;
        if (users != board.users) return false;
        if (status != board.status) return false;
        if (title != null ? !title.equals(board.title) : board.title != null) return false;
        if (voting != board.voting) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + users;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (voting != null ? voting.hashCode() : 0);
        return result;
    }
}
