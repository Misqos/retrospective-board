package pl.edu.agh.jtp2.retrospective.domain.board;

/**
 * Created by misqos on 05.08.14.
 */
public enum Status {
    ADDING("Adding notes"),
    VOTING("Voting for notes"),
    CLOSED("Board closed");

    private String description;

    private Status(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
