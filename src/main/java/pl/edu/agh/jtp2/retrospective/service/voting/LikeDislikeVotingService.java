package pl.edu.agh.jtp2.retrospective.service.voting;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.voting.VotingDAO;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;

/**
 * Created by misqos on 13.08.14.
 */
public class LikeDislikeVotingService implements VotingService {
    @Autowired
    NoteGroupService noteGroupService;

    @Autowired
    VotingDAO votingDAO;

    public void setVotingDAO(VotingDAO votingDAO) {
        this.votingDAO = votingDAO;
    }

    @Override
    public void upVote(User user, NoteGroup group) {
        group.voteUp();
        noteGroupService.update(group);
        votingDAO.markUpVote(user, group);
    }

    @Override
    public void downVote(User user, NoteGroup group) {
        group.voteDown();
        noteGroupService.update(group);
        votingDAO.markDownVote(user, group);
    }

    @Override
    public boolean possibleDownVote() {
        return true;
    }

    @Override
    public boolean canUpVote(User user, NoteGroup group) {
        return !votingDAO.isTouched(user, group);
    }

    @Override
    public boolean canDownVote(User user, NoteGroup group) {
        return !votingDAO.isTouched(user, group);
    }

    @Override
    public void reset(User user, NoteGroup group) {
        group.setPriority(group.getPriority() - votingDAO.getUserVotes(user, group));
        noteGroupService.update(group);
        votingDAO.reset(user, group);
    }

    @Override
    public void upVote(User user, int times, NoteGroup group) {
        throw new UnsupportedOperationException("Unsupported operation for this voting strategy");
    }

    @Override
    public boolean upVoted(User user, NoteGroup group) {
        return votingDAO.isUpVoted(user, group);
    }

    @Override
    public boolean downVoted(User user, NoteGroup group) {
        return !votingDAO.isUpVoted(user, group);
    }

    @Override
    public boolean isTouched(User user, NoteGroup group) {
        return votingDAO.isTouched(user, group);
    }

    @Override
    public int getUserVotes(User user) {
        return votingDAO.getUserVotes(user);
    }
}
