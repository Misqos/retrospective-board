package pl.edu.agh.jtp2.retrospective.service.voting;

import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;

/**
 * Created by misqos on 05.08.14.
 */
public interface VotingService {
    public void upVote(User user, NoteGroup group);

    public void downVote(User user, NoteGroup group);

    public boolean possibleDownVote();

    public boolean canUpVote(User user, NoteGroup group);

    public boolean canDownVote(User user, NoteGroup group);

    public void reset(User user, NoteGroup group);

    public void upVote(User user, int times, NoteGroup group);

    public boolean upVoted(User user, NoteGroup group);

    public boolean downVoted(User user, NoteGroup group);

    public boolean isTouched(User user, NoteGroup group);

    public int getUserVotes(User user);
}
