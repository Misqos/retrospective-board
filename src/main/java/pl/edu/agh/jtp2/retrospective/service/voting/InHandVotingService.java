package pl.edu.agh.jtp2.retrospective.service.voting;

import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.jtp2.retrospective.dao.voting.VotingDAO;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;

/**
 * Created by misqos on 13.08.14.
 */
public class InHandVotingService implements VotingService {
    @Autowired
    VotingDAO votingDAO;

    @Autowired
    NoteGroupService noteGroupService;

    public void setVotingDAO(VotingDAO votingDAO) {
        this.votingDAO = votingDAO;
    }

    @Override
    public void upVote(User user, NoteGroup group) {
        group.voteUp();
        noteGroupService.update(group);
        votingDAO.markUpVote(user, group);
    }

    @Override
    public void downVote(User user, NoteGroup group) {
        throw new UnsupportedOperationException("This operation is unsupported in this voting strategy");
    }

    @Override
    public boolean possibleDownVote() {
        return false;
    }

    @Override
    public boolean canUpVote(User user, NoteGroup group) {
        return votingDAO.getUserVotes(user) < 20; //Magic number.
    }

    @Override
    public boolean canDownVote(User user, NoteGroup group) {
        return false;
    }

    @Override
    public void reset(User user, NoteGroup group) {
        group.setPriority(group.getPriority() - votingDAO.getUserVotes(user, group));
        noteGroupService.update(group);
        votingDAO.reset(user, group);
    }

    @Override
    public void upVote(User user, int times, NoteGroup group) {
        group.setPriority(group.getPriority() + times);
        noteGroupService.update(group);
        votingDAO.markUpVote(user, times, group);
    }

    @Override
    public boolean upVoted(User user, NoteGroup group) {
        return votingDAO.getUserVotes(user, group) > 0;
    }

    @Override
    public boolean downVoted(User user, NoteGroup group) {
        return false;
    }

    @Override
    public boolean isTouched(User user, NoteGroup group) {
        return votingDAO.isTouched(user, group);
    }

    @Override
    public int getUserVotes(User user) {
        return votingDAO.getUserVotes(user);
    }
}
