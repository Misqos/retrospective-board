package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.edu.agh.jtp2.retrospective.dao.board.BoardDAO;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;

/**
 * Created by misqos on 04.08.14.
 */
@Controller
public class HomeController {
    @Autowired
    BoardDAO boardDAO;

    @RequestMapping(value = "/")
    public String printHomepage(ModelMap model) {
        model.addAttribute("command", new Board());
        model.addAttribute("pagetitle", "Retrospective Board Homepage");
        return "home";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String goToBoard(@RequestParam("token") String token) {
        return "redirect:/board/" + token;
    }
}
