package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;
import pl.edu.agh.jtp2.retrospective.domain.board.Status;
import pl.edu.agh.jtp2.retrospective.domain.voting.VotingStrategies;
import pl.edu.agh.jtp2.retrospective.service.board.BoardService;
import pl.edu.agh.jtp2.retrospective.service.token.TokenMachine;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by misqos on 11.08.14.
 */
@Controller
public class BoardManagementController {
    @Autowired
    BoardService boardService;

    @Autowired
    UserService userService;

    @Autowired
    TokenMachine tokenMachine;

    private boolean isPermitted(String token) {
        return token.startsWith("A");
    }

    @RequestMapping(value = "/board/{token}/state")
    public String state(@PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        model.addAttribute("token", token);
        model.addAttribute("states", Status.values());
        model.addAttribute("pagetitle", "Retrospective Board - board management");
        return "changestate";
    }

    @RequestMapping(value = "/board/{token}/state", method = RequestMethod.POST)
    public String changeState(@RequestParam("state") Status status, @PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        final Board board = boardService.get(userService.get(token).getBoardId());
        board.setStatus(status);
        boardService.update(board);
        return "redirect:/board/" + token;
    }

    @RequestMapping(value = "/board/{token}/keys")
    public String keys(@PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        User admin = userService.get(token);
        List<User> users = userService.getByBoardId(admin.getBoardId());
        users.remove(admin);
        model.addAttribute("token", token);
        model.addAttribute("users", users);
        model.addAttribute("pagetitle", "Retrospective Board - board management");
        return "managekeys";
    }

    @RequestMapping(value = "/board/{token}/keys", method = RequestMethod.POST)
    public String newKeys(@RequestParam("keys") int keys, @PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        Board board = boardService.get(userService.get(token).getBoardId());
        board.setUsers(board.getUsers() + keys);
        getUsers(board, keys);
        return "redirect:/board/" + token + "/keys";
    }

    @RequestMapping(value = "/board/{token}/edit")
    public String edit(@PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        model.addAttribute("btitle", boardService.get(userService.get(token).getBoardId()).getTitle());
        model.addAttribute("enums", VotingStrategies.values());
        model.addAttribute("token", token);
        model.addAttribute("pagetitle", "Retrospective Board - board management");
        return "editboard";
    }

    @RequestMapping(value = "/board/{token}/edit", method = RequestMethod.POST)
    public String updateBoard(@RequestParam("title") String title, @RequestParam("voting") VotingStrategies voting,
                              @PathVariable String token, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        Board b2 = boardService.get(userService.get(token).getBoardId());
        b2.setTitle(title);
        b2.setVoting(voting);
        boardService.update(b2);
        return "redirect:/board/" + token;
    }

    @RequestMapping(value = "/creation", method = RequestMethod.POST)
    public String boardCreation(@ModelAttribute Board board, ModelMap model) {
        model.addAttribute("title", board.getTitle());
        model.addAttribute("enums", VotingStrategies.values());
        model.addAttribute("command", board);
        model.addAttribute("pagetitle", "Retrospective Board - board creation");

        return "creation";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String newBoard(@ModelAttribute Board board, ModelMap model) {
        this.insertBoard(board);
        List<String> userTokens = this.getUsers(board);
        String adminToken = this.getAdmin(board);
        model.addAttribute("userTokens", userTokens);
        model.addAttribute("adminToken", adminToken);
        return "tokens";
    }

    private String getAdmin(Board board) {
        String token = tokenMachine.getAdminToken();
        User admin = new User(userService.nextId(), token, board.getId(), "");
        userService.insert(admin);
        return token;
    }

    private List<String> getUsers(Board board) {
        int users = board.getUsers();
        List<String> userTokens = new ArrayList<>();
        for (int i = 0; i < users; i++) {
            String token = tokenMachine.getUserToken();
            User user = new User(userService.nextId(), token, board.getId(), "");
            userService.insert(user);
            userTokens.add(token);
        }
        return userTokens;
    }

    private List<String> getUsers(Board board, int users) {
        List<String> userTokens = new ArrayList<>();
        for (int i = 0; i < users; i++) {
            String token = tokenMachine.getUserToken();
            User user = new User(userService.nextId(), token, board.getId(), "");
            userService.insert(user);
            userTokens.add(token);
        }
        return userTokens;
    }

    private void insertBoard(Board board) {
        board.setId(boardService.nextId());
        boardService.insert(board);
    }
}
