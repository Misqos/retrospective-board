package pl.edu.agh.jtp2.retrospective.domain;

/**
 * Created by misqos on 05.08.14.
 */
public class User {
    private int id;
    private String key;
    private int boardId;
    private String name;

    public User() {
    }

    public User(int id, String key, int boardId, String name) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.boardId = boardId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (boardId != user.boardId) return false;
        if (id != user.id) return false;
        if (key != null ? !key.equals(user.key) : user.key != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + boardId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
