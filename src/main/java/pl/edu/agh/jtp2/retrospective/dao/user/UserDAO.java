package pl.edu.agh.jtp2.retrospective.dao.user;

import pl.edu.agh.jtp2.retrospective.domain.User;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public interface UserDAO {
    public void insert(User user);

    public List<User> findAll();

    public User findById(int id);

    public List<User> findByBoardId(int id);

    public User findByToken(String token);

    public List<User> findAllFromNewest();

    public void update(User user);
}
