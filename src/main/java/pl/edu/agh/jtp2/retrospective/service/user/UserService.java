package pl.edu.agh.jtp2.retrospective.service.user;

import pl.edu.agh.jtp2.retrospective.domain.User;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public interface UserService {
    public int nextId();

    public void insert(User user);

    public List<User> get();

    public User get(int id);

    public User get(String userToken);

    public List<User> getByBoardId(int id);

    public int getBoardId(String userToken);

    public int getBoardId(int userId);

    public void update(User user);
}
