package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.Note;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNote;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNoteGroup;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteService;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by misqos on 10.08.14.
 */
@Controller
public class NoteController {
    @Autowired
    NoteGroupService noteGroupService;

    @Autowired
    NoteService noteService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/board/{token}/add")
    public String newNote(@PathVariable String token, ModelMap model) {
        model.addAttribute("token", token);
        model.addAttribute("type", NoteType.values());
        model.addAttribute("command", new NoteFormObject());
        model.addAttribute("pagetitle", "Retrospective Board - new note");
        return "newnote";
    }

    @RequestMapping(value = "/board/{token}/add", method = RequestMethod.POST)
    public String addNote(@PathVariable String token, @ModelAttribute NoteFormObject ob, ModelMap model) {
        int groupId = noteGroupService.nextId();
        User user = userService.get(token);
        final NoteGroup group = new NoteGroup(groupId, user.getBoardId(), ob.getType(), 0);
        noteGroupService.insert(group);
        final Note note = new Note(noteService.nextId(), groupId, user.getId(), ob.getType(), ob.getText());
        noteService.insert(note);
        return "redirect:/board/" + token;
    }

    @RequestMapping(value = "/board/{token}/add/{groupId}")
    public String newSimilar(@PathVariable String token, @PathVariable int groupId, ModelMap model) {
        model.addAttribute("token", token);
        model.addAttribute("groupId", groupId);
        model.addAttribute("command", new NoteFormObject());
        model.addAttribute("pagetitle", "Retrospective Board - new similar");
        return "newsimilar";
    }

    @RequestMapping(value = "/board/{token}/add/{groupId}", method = RequestMethod.POST)
    public String addSimilar(@PathVariable String token, @PathVariable int groupId, @ModelAttribute NoteFormObject ob, ModelMap model) {
        NoteGroup noteGroup = noteGroupService.getSingle(groupId);
        User user = userService.get(token);
        final Note note = new Note(noteService.nextId(), groupId, user.getId(), noteGroup.getType(), ob.getText());
        noteService.insert(note);
        return "redirect:/board/" + token;
    }

    @RequestMapping(value = "/board/{token}/merge/{groupId}")
    public String merge(@PathVariable String token, @PathVariable int groupId, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        NoteGroup group = noteGroupService.getSingle(groupId);
        List<NoteGroup> groups = noteGroupService.get(group.getType(), group.getBoardId());
        groups.remove(group);
        FrontNoteGroup fnoteg = new FrontNoteGroup(group); //TODO Fucked up here.
        fnoteg.setNotes(noteService.get(group.getId()).stream().map(note ->
                new FrontNote(note, userService)).collect(Collectors.toList()));
        List<FrontNoteGroup> fnotegs = new ArrayList<>();
        for (NoteGroup g : groups) {
            FrontNoteGroup fnotegr = new FrontNoteGroup(g);
            List<FrontNote> nots = noteService.get(g.getId()).stream().map(note ->
                    new FrontNote(note, userService)).collect(Collectors.toList());
            fnotegr.setNotes(nots);
            fnotegs.add(fnotegr);
        }
        model.addAttribute("groups", fnotegs);
        model.addAttribute("group", fnoteg);
        model.addAttribute("noNotes", fnotegs.isEmpty());
        model.addAttribute("token", token);
        model.addAttribute("id", groupId);
        model.addAttribute("pagetitle", "Retrospective Board - note group merging");
        return "merge";
    }

    @RequestMapping(value = "/board/{token}/merge/{group1}/{group2}")
    public String doMerge(@PathVariable String token, @PathVariable int group1,
                          @PathVariable int group2, ModelMap model) {
        if (!isPermitted(token)) return "redirect:/board/" + token;
        NoteGroup first = noteGroupService.getSingle(group1);
        NoteGroup second = noteGroupService.getSingle(group2);
        noteGroupService.insert(noteGroupService.merge(first, second));

        return "redirect:/board/" + token;
    }

    private boolean isPermitted(String token) {
        return token.startsWith("A");
    }

    public static final class NoteFormObject {
        private NoteType type;
        private String text;

        public NoteType getType() {
            return type;
        }

        public void setType(NoteType type) {
            this.type = type;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
