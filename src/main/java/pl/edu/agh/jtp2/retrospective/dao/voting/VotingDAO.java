package pl.edu.agh.jtp2.retrospective.dao.voting;

import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;

/**
 * Created by misqos on 13.08.14.
 */
public interface VotingDAO {
    public void markUpVote(User user, NoteGroup group);

    public void markDownVote(User user, NoteGroup group);

    public void markUpVote(User user, int times, NoteGroup group);

    public void reset(User user, NoteGroup group);

    public boolean isTouched(User user, NoteGroup group);

    public int getUserVotes(User user, NoteGroup group);

    public int getUserVotes(User user);

    public boolean isUpVoted(User user, NoteGroup group);
}
