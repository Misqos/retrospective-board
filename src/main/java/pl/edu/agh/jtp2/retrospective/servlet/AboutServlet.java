package pl.edu.agh.jtp2.retrospective.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by misqos on 26.08.14.
 */
public class AboutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        String text =
                "<h1>Retrospective Board</h1>\n" +
                        "<p>Simple app for scrum retrospective</p>\n" +
                        "<p>Using:</p>\n" +
                        "<ul>\n" +
                        "<li>\n" +
                        "<p>Java 8</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>Maven</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>SpringMVC</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>MySQL</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>Tiles</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>INK</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>FontAwesome</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>Cargo</p>\n" +
                        "</li>\n" +
                        "<li>\n" +
                        "<p>Junit4</p>\n" +
                        "</li>\n" +
                        "</ul>\n" +
                        "<h1>Database configuration</h1>\n" +
                        "<p>Whole database connection config can be found in src/main/webapp/WEB-INF/mvc-dispacher-serverlet.xml in dataSource bean.</p>\n" +
                        "<p>There is schema.sql file attached which consist of query which will create \"retrospective\" database with all required tables for you.</p>\n" +
                        "<p>Of course you can use every SQL database, but remember to change config in abovementioned file to use proper driver.</p>\n" +
                        "<h1>Deployment</h1>\n" +
                        "<p>Best way to deploy this project is to have Tomcat8 already installed and working.</p>\n" +
                        "<p>If you are using Maven you can deploy this app simply by typing mvn cargo:run. App will be accesible at <a href=\"http://localhost:8080/retrospective/\" rel=\"nofollow\"  >http://localhost:8080/retrospective/</a></p>\n" +
                        "<h1>Final notes</h1>\n" +
                        "<p>This app is not completed. It has a lot of bugs and sh**ty code. There are some things that might not work for now. I brought this app to this stage in 10 days and I am really proud of myself learning how to do it in such time.</p>\n";
        out.println(text);
        out.close();
    }
}
