package pl.edu.agh.jtp2.retrospective.dao.voting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by misqos on 13.08.14.
 */
public class VotingDAOImpl implements VotingDAO {
    @Autowired
    DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void markUpVote(User user, NoteGroup group) {
        final int userVotes = getUserVotes(user, group);
        reset(user, group);
        String sql = "INSERT INTO LikeDislike " +
                "(groupId, userId, typo, times) VALUES (?, ?, ?, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getId(), user.getId(), "LIKE", userVotes + 1);
    }

    @Override
    public void markDownVote(User user, NoteGroup group) {
        String sql = "INSERT INTO LikeDislike " +
                "(groupId, userId, typo, times) VALUES (?, ?, ?, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getId(), user.getId(), "DISLIKE", 1);
    }

    @Override
    public void markUpVote(User user, int times, NoteGroup group) {
        String sql = "INSERT INTO LikeDislike " +
                "(groupId, userId, typo, times) VALUES (?, ?, ?, ?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getId(), user.getId(), "LIKE", times);
    }

    @Override
    public void reset(User user, NoteGroup group) {
        String sql = "DELETE FROM LikeDislike WHERE groupId = ? AND userId = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, group.getId(), user.getId());
    }

    @Override
    public boolean isTouched(User user, NoteGroup group) {
        String sql = "SELECT * FROM LikeDislike WHERE groupId = ? AND userId = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        return !jdbcTemplate.queryForList(sql, group.getId(), user.getId()).isEmpty();
    }

    @Override
    public int getUserVotes(User user, NoteGroup group) {
        String sql = "SELECT * FROM LikeDislike WHERE groupId = ? AND userId = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        int votes = 0;
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, group.getId(), user.getId());

        for (Map row : rows) {
            int times = Integer.parseInt(String.valueOf(row.get("times")));
            if (row.get("typo").equals("LIKE")) {
                votes += times;
            } else {
                votes -= times;
            }
        }

        return votes;
    }

    @Override
    public int getUserVotes(User user) {
        String sql = "SELECT * FROM LikeDislike WHERE userId = ? AND typo = 'LIKE'";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        int votes = 0;

        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, user.getId());
        for (Map map : maps) {
            votes += Integer.parseInt(String.valueOf(map.get("times")));
        }

        return votes;
    }

    @Override
    public boolean isUpVoted(User user, NoteGroup group) {
        String sql = "SELECT * FROM LikeDislike WHERE groupId = ? AND userId = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, group.getId());
            ps.setInt(2, user.getId());
            ResultSet rs = ps.executeQuery();
            boolean result = rs.next() && rs.getString("typo").equals("LIKE");
            rs.close();
            ps.close();
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
