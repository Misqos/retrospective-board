package pl.edu.agh.jtp2.retrospective.service.token;

/**
 * Created by misqos on 09.08.14.
 */
public interface TokenMachine {
    public String getAdminToken();

    public String getUserToken();
}
