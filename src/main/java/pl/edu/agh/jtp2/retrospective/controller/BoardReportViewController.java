package pl.edu.agh.jtp2.retrospective.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.agh.jtp2.retrospective.domain.User;
import pl.edu.agh.jtp2.retrospective.domain.board.Board;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteGroup;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNote;
import pl.edu.agh.jtp2.retrospective.helpers.FrontNoteGroup;
import pl.edu.agh.jtp2.retrospective.service.board.BoardService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteGroupService;
import pl.edu.agh.jtp2.retrospective.service.note.NoteService;
import pl.edu.agh.jtp2.retrospective.service.user.UserService;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by misqos on 14.08.14.
 */
@Controller
public class BoardReportViewController {
    @Autowired
    UserService userService;

    @Autowired
    BoardService boardService;

    @Autowired
    NoteGroupService noteGroupService;

    @Autowired
    NoteService noteService;

    @RequestMapping(value = "/board/{token}/report")
    public String report(@PathVariable String token, ModelMap model) {
        User user = userService.get(token);
        Board board = boardService.get(user.getBoardId());

        List<NoteGroup> bgroups = noteGroupService.get(board.getId());
        Set<FrontNoteGroup> glads = new TreeSet<>();
        Set<FrontNoteGroup> sads = new TreeSet<>();
        Set<FrontNoteGroup> ideas = new TreeSet<>();
        for (NoteGroup g : bgroups) {
            FrontNoteGroup group = new FrontNoteGroup(g);
            List<FrontNote> nots = noteService.get(g.getId()).stream().map(note ->
                    new FrontNote(note, userService)).collect(Collectors.toList());
            group.setNotes(nots);
            switch (group.getType()) {
                case GLAD:
                    glads.add(group);
                    break;
                case SAD:
                    sads.add(group);
                    break;
                case NEW_IDEA:
                    ideas.add(group);
                    break;
            }
        }

        model.addAttribute("noGlads", glads.isEmpty());
        model.addAttribute("noSads", sads.isEmpty());
        model.addAttribute("noIdeas", ideas.isEmpty());

        model.addAttribute("glads", glads);
        model.addAttribute("sads", sads);
        model.addAttribute("ideas", ideas);

        model.addAttribute("participants", userService.getByBoardId(board.getId()).size());
        model.addAttribute("nnotes", noteGroupService.get(board.getId()).size());

        model.addAttribute("board", board);
        model.addAttribute("pagetitle", "Retrospective Board - board report");
        return "report";
    }
}
