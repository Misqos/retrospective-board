package pl.edu.agh.jtp2.retrospective.dao.note;

import pl.edu.agh.jtp2.retrospective.domain.note.Note;

import java.util.List;

/**
 * Created by misqos on 09.08.14.
 */
public interface NoteDAO {
    public void insert(Note note);

    public Note findById(int id);

    public List<Note> findByGroupId(int groupId);

    public List<Note> findAllFromNewest();

    public void update(Note note);
}
