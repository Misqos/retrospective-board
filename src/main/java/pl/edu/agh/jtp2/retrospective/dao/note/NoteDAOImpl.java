package pl.edu.agh.jtp2.retrospective.dao.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.edu.agh.jtp2.retrospective.domain.note.Note;
import pl.edu.agh.jtp2.retrospective.domain.note.NoteType;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by misqos on 09.08.14.
 */
public class NoteDAOImpl implements NoteDAO {
    @Autowired
    DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void insert(Note note) {
        String sql = "INSERT INTO Notes " +
                "(id, groupId, userId, typo, text) VALUES (?, ?, ?, ?, ?)";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, note.getId());
            ps.setInt(2, note.getGroupId());
            ps.setInt(3, note.getUserId());
            ps.setString(4, String.valueOf(note.getNoteType()));
            ps.setString(5, note.getText());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Note findById(int id) {
        String sql = "SELECT * FROM Notes WHERE id = ?";
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            Note note = null;
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                note = new Note(
                        rs.getInt("id"),
                        rs.getInt("groupId"),
                        rs.getInt("userId"),
                        Enum.valueOf(NoteType.class, rs.getString("typo")),
                        rs.getString("text")
                );
            }
            rs.close();
            ps.close();
            return note;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Note> findByGroupId(int groupId) {
        String sql = "SELECT * FROM Notes WHERE groupId = ? ORDER BY id";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Note> notes = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, groupId);
        for (Map row : rows) {
            Note note = new Note(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("groupId"))),
                    Integer.parseInt(String.valueOf(row.get("userId"))),
                    Enum.valueOf(NoteType.class, String.valueOf(row.get("typo"))),
                    String.valueOf(row.get("text"))
            );
            notes.add(note);
        }
        return notes;
    }

    @Override
    public List<Note> findAllFromNewest() {
        String sql = "SELECT * FROM Notes ORDER BY id DESC";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        List<Note> notes = new ArrayList<>();
        final List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            Note note = new Note(
                    Integer.parseInt(String.valueOf(row.get("id"))),
                    Integer.parseInt(String.valueOf(row.get("groupId"))),
                    Integer.parseInt(String.valueOf(row.get("userId"))),
                    Enum.valueOf(NoteType.class, String.valueOf(row.get("typo"))),
                    String.valueOf(row.get("text"))
            );
            notes.add(note);
        }
        return notes;
    }

    @Override
    public void update(Note note) {
        String sql = "UPDATE Notes SET groupId = ?, userId = ?, typo = ?, text = ? WHERE id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(sql, note.getGroupId(), note.getUserId(), String.valueOf(note.getNoteType()), note.getText(), note.getId());
    }
}
