<footer class="clearfix">
    <div class="ink-grid">
        <ul class="unstyled inline half-vertical-space">
            <li><a href="${pageContext.request.contextPath}/about">About</a></li>
        </ul>
        <p class="note">Copyright Misqos &copy; 2014</p>
    </div>
</footer>