<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>${pagetitle}</title>
    <meta name="description" content="">
    <meta name="author" content="ink, cookbook, recipes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- Place favicon.ico and apple-touch-icon(s) here  -->

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">

    <!-- load inks CSS -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/ink-flex.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/own.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

    <!-- load inks CSS for IE8 -->
    <!--[if lt IE 9 ]>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ink-ie.min.css" type="text/css"
          media="screen" title="no title" charset="utf-8">
    <![endif]-->

    <!-- test browser flexbox support and load legacy grid if unsupported -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script>
    <script type="text/javascript">
        Modernizr.load({
            test: Modernizr.flexbox,
            nope: 'resources/css/ink-legacy.min.css'
        });
    </script>

    <!-- load inks javascript files -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/holder.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ink-all.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/autoload.js"></script>

</head>

<body>
<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="footer"/>
</body>
</html>