<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>Your new board created!</h1>

                <p>Admin token allows you to manage your board. Enter your key on Homepage in order to participate and
                    manage your board.</p>

                <p>Your admin token: <span class="fw-500">${adminToken}</span></p>

                <p>These are access tokens for users. One person should use one key.</p>
                <ul class="unstyled">
                    <c:forEach var="t" items="${userTokens}">
                        <li>${t}</li>
                    </c:forEach>
                </ul>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>