<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <div class="column-group gutters">
                    <div class="all-80">
                        <h1>${title}</h1>
                        <a href="${pageContext.request.contextPath}/board/${token}/add"
                           class="ink-button green gutters"><i class="fa fa-fw fa-file"></i>&nbsp; Add new note!</a>
                        <br><br><br>
                        <c:if test="${noNotes}"><h2>No notes to display!</h2></c:if>
                        <div class="content gutters">
                            <c:forEach items="${groups}" var="groups">
                                <div class="noteg gutters">
                                    <span class="condensed-700 fw-500 ink-label ${groups.type.color}">${groups.type.description}</span>
                                    <c:forEach items="${groups.notes}" var="note">
                                        <blockquote><p>${note.text}</p>
                                            <cite class="small">added by ${note.author}</cite>
                                        </blockquote>
                                    </c:forEach>
                                    <div class="note">
                                        <p>
                                            <a class="ink-button small"
                                               href="${pageContext.request.contextPath}/board/${token}/add/${groups.id}"><i
                                                    class="fa fa-fw fa-file"></i>&nbsp; Add similar</a>
                                            <c:if test="${admin}">
                                                <a class="ink-button small"
                                                   href="${pageContext.request.contextPath}/board/${token}/merge/${groups.id}"><i
                                                        class="fa fa-fw fa-magic"></i>&nbsp; Merge</a>
                                            </c:if>
                                        </p>
                                    </div>
                                </div>
                                <br>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="all-20">
                        <div class="ink-navigation all-100">
                            <ul class="dropdown">
                                <li>&nbsp; <i class="fa fa-fw fa-user"></i>&nbsp; ${username}</li>
                                <li>&nbsp; <i class="fa fa-fw fa-key"></i>&nbsp; ${token}</li>
                                <li class="separator-above"><a
                                        href="${pageContext.request.contextPath}/user/${token}/name">Change username</a>
                                </li>
                                <c:if test="${admin}">
                                    <li class="separator-above"><a
                                            href="${pageContext.request.contextPath}/board/${token}/state">Change board
                                        state</a></li>
                                    <li><a href="${pageContext.request.contextPath}/board/${token}/keys">Manage user
                                        keys</a></li>
                                    <li><a href="${pageContext.request.contextPath}/board/${token}/edit">Edit board
                                        details</a></li>
                                </c:if>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>