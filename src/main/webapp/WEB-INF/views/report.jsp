<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>${board.title}</h1>

                <h2>Board closed!</h2>

                <p>Your board is closed now. Now you can see top voted notes in categories.</p>

                <div class="content gutters">
                    <div class="column-group gutters">
                        <div class="all-33">
                            <span class="ink-label red">Sads</span><br>
                            <c:forEach items="${sads}" var="sad">
                                <div class="noteg gutters">
                                    <div class="column-group">
                                        <div class="all-10">
                                            <div class="align-center push-center">
                                                <strong>${sad.priority}</strong>
                                                <small>votes</small>
                                            </div>
                                        </div>
                                        <div class="all-90">
                                            <c:forEach items="${sad.notes}" var="note">
                                                <blockquote><p>${note.text}</p>
                                                    <cite class="small">added by ${note.author}</cite>
                                                </blockquote>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="all-33">
                            <span class="ink-label blue">New ideas</span><br>
                            <c:forEach items="${ideas}" var="sad">
                                <div class="noteg gutters">
                                    <div class="column-group">
                                        <div class="all-10">
                                            <div class="align-center push-center">
                                                <strong>${sad.priority}</strong>
                                                <small>votes</small>
                                            </div>
                                        </div>
                                        <div class="all-90">
                                            <c:forEach items="${sad.notes}" var="note">
                                                <blockquote><p>${note.text}</p>
                                                    <cite class="small">added by ${note.author}</cite>
                                                </blockquote>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="all-33">
                            <span class="ink-label green">Glads</span><br>
                            <c:forEach items="${glads}" var="glad">
                                <div class="noteg gutters">
                                    <div class="column-group">
                                        <div class="all-10">
                                            <div class="align-center push-center">
                                                <strong>${glad.priority}</strong>
                                                <small>votes</small>
                                            </div>
                                        </div>
                                        <div class="all-90">
                                            <c:forEach items="${glad.notes}" var="note">
                                                <blockquote><p>${note.text}</p>
                                                    <cite class="small">added by ${note.author}</cite>
                                                </blockquote>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <h2>Statistics</h2>
                    <ul class="unstyled">
                        <li><i class="fa fa-fw fa-users"></i>&nbsp; Number of participants: ${participants}</li>
                        <li><i class="fa fa-fw fa-file"></i>&nbsp; Number of notes groups: ${nnotes}</li>
                    </ul>

                </div>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>