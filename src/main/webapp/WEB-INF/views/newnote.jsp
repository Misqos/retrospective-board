<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>Add new note</h1>
                <form:form action="${pageContext.request.contextPath}/board/${token}/add" method="POST"
                           class="ink-form column-group gutters push-center">
                    <fieldset class="all-33 small-100 tiny-100">
                        <legend>Enter details</legend>
                        <div class="control-group">
                            <p class="label">Note type</p>
                            <ul class="control unstyled">
                                <c:forEach var="t" items="${type}">
                                    <li><form:radiobutton path="type" id="${t}" value="${t}"/><label
                                            for="${t}">${t.description}</label></li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="control-group">
                            <form:label path="text">Text</form:label>
                            <div class="control">
                                <form:input type="text" path="text"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control">
                                <input type="submit" value="Submit" class="ink-button green"/>
                            </div>
                        </div>
                    </fieldset>
                </form:form>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>