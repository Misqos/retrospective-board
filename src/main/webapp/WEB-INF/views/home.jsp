<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            <![endif]-->

            <!-- Add your site or application content here -->

            <header class="vertical-space">
                <h1>Retrospective Board
                    <small>as simple as it has to be</small>
                </h1>
            </header>
            <div class="column-group gutters">
                <div class="all-100 welcome">
                    <div style="text-align: center; width:100%"><i class="fa fa-5x fa-users"></i></div>
                    <h3>Be more productive!</h3>
                    <!--<i class="fa fa-slack fa-3x"></i>-->
                    <p>No matter how good a Scrum team is, there is always opportunity to improve. Although a good Scrum
                        team will be constantly looking for improvement opportunities, the team should set aside a
                        brief, dedicated period at the end of each sprint to deliberately reflect on how they are doing
                        and to find ways to improve. This occurs during the sprint retrospective.</p>
                </div>
                <div class="all-100 info">
                    <div style="text-align: center; width:100%"><i class="fa fa-5x fa-file"></i></div>
                    <h3>Make things easier with Retrospective Board!</h3>

                    <p>Retrospective Board is ultimate solution which will improve your sprint retrospective. Start
                        using it right now!</p>
                </div>
            </div>
            <div class="column-group gutters">
                <div class="all-50 small-100 tiny-100">
                    <h3>Create new Retrospective Board</h3>
                    <form:form class="ink-form" method="POST" action="${pageContext.request.contextPath}/creation">
                        <div class="control-group column-group gutters">
                            <form:label path="title" class="all-20 align-right">Title:</form:label>
                            <div class="control all-80">
                                <form:input path="title" type="text" placeholder="Entitle your board..."/>
                                <p class="tip">This title will be displayed for all board participants.</p>
                                <input class="ink-button green" type="submit" value="Create new board!"/>
                            </div>
                        </div>
                    </form:form>
                </div>
                <div class="all-50 small-100 tiny-100">
                    <h3>Access existing board</h3>

                    <form action="${pageContext.request.contextPath}/" method="POST" class="ink-form">
                        <div class="control-group column-group gutters">
                                <%--@declare id="token"--%><label for="token" class="all-20 align-right">Token:</label>

                            <div class="control all-80">
                                <input id="token" name="token" type="text" placeholder="Your token goes here..."/>

                                <p class="tip">Insert your admin token or user token delivered by your manager.</p>
                                <input class="ink-button green" type="submit" value="Access your board!"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>