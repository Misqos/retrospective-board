<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/board/${token}">Return to board</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>Groups merge</h1>

                <h2>Group to merge:</h2>

                <div class="noteg gutters">
                    <span class="condensed-700 fw-500 ink-label ${group.type.color}">${group.type.description}</span>
                    <c:forEach items="${group.notes}" var="note">
                        <blockquote><p>${note.text}</p>
                            <cite class="small">added by ${note.author}</cite>
                        </blockquote>
                    </c:forEach>
                </div>
                <h2>Select one to merge with</h2>
                <c:if test="${noNotes}"><h2>No notes to display!</h2></c:if>
                <div class="content gutters">
                    <c:forEach items="${groups}" var="groups">
                        <div class="noteg gutters">
                            <span class="condensed-700 fw-500 ink-label ${groups.type.color}">${groups.type.description}</span>
                            <a class="fw-500 ink-button orange small"
                               href="${pageContext.request.contextPath}/board/${token}/merge/${id}/${groups.id}">Merge
                                with this!</a>
                            <c:forEach items="${groups.notes}" var="note">
                                <blockquote><p>${note.text}</p>
                                    <cite class="small">added by ${note.author}</cite>
                                </blockquote>
                            </c:forEach>
                        </div>
                        <br>
                    </c:forEach>
                </div>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>