<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>Set your name</h1>

                <p>Your name will be displayed for other board participants to identify your Notes</p>

                <form class="ink-form column-group gutters push-center" method="POST"
                      action="${pageContext.request.contextPath}/user/update">
                    <fieldset class="all-33 small-100 tiny-100">
                        <div class="control-group">
                                <%--@declare id="name"--%><label for="name">Name</label>

                            <div class="control">
                                <input type="text" id="name" name="name"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control">
                                <input type="submit" value="Submit" class="ink-button green"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>