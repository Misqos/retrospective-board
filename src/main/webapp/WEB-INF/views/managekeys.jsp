<%--
  Created by IntelliJ IDEA.
  User: misqos
  Date: 08.08.14
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <!--[if lte IE 9 ]>
        <div class="ink-grid">
        <div class="ink-alert basic">
        <button class="ink-dismiss">&times;</button>
        <p>
        <strong>You are using an outdated Internet Explorer version.</strong>
        Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
        </p>
        </div>
        </div>
        <![endif]-->

        <div class="wrap">
            <div class="top-menu">
                <nav class="ink-navigation ink-grid">
                    <ul class="menu horizontal black">
                        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/board/${token}">Return to board</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ink-grid vertical-space">
                <h1>User keys</h1>
                <table class="ink-table">
                    <thead>
                    <tr>
                        <td>Key</td>
                        <td>Username</td>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${users}" var="u">
                        <tr>
                            <td>${u.key}</td>
                            <td>${u.name}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <br><br>

                <form class="ink-form column-group gutters" method="POST"
                      action="${pageContext.request.contextPath}/board/${token}/keys">
                    <fieldset class="all-33 small-100 tiny-100">
                        <legend>Generate new user keys</legend>
                        <div class="control-group">
                                <%--@declare id="keys"--%><label for="keys">Number of keys</label>

                            <div class="control">
                                <input type="number" id="keys" name="keys"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control">
                                <input type="submit" value="Generate" class="ink-button green"/>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="push"></div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>