Retrospective Board
=======================

Simple app for scrum retrospective

Using:

- Java 8

- Maven

- SpringMVC

- MySQL

- Tiles

- INK

- FontAwesome

- Cargo

- Junit4

Database configuration
=====================
Whole database connection config can be found in src/main/webapp/WEB-INF/mvc-dispacher-serverlet.xml in dataSource bean.

There is schema.sql file attached which consist of query which will create "retrospective" database with all required tables for you.

Deployment
==================
Best way to deploy this project is to have Tomcat8 already installed and working.

If you are using Maven you can deploy this app simply by typing mvn cargo:run. App will be accesible at http://localhost:8080/retrospective/

Final notes
=================
This app is not completed. It has a lot of bugs and sh**ty code. There are some things that might not work for now. I brought this app to this stage in 10 days and I am really proud of myself learning how to do it in such time.

Contribution
===============
If you want to contribute to this app, let me know ;)